package database

// DB Gorm DB
import (
	"log"

	"test/test_app/models"

	"github.com/jinzhu/gorm"
)

var DB *gorm.DB

func init() {
	db := "host=localhost user=postgres password=postgres dbname=test sslmode=disable"
	// *Config.Adapter = "postgresql"
	log.Printf("connecting to pg, conf: %v", db)
	DB, err := gorm.Open("postgres", db)
	if err != nil {
		panic(err)
	}
	DB.DB().SetMaxOpenConns(20)
	models.DB = DB
}
