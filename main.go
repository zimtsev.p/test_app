package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	ransack "test/go-ransack"
	"test/test_app/controllers"
	"test/test_app/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
)

var config *ransack.Configuration

func databaseInit() {
	db := "host=localhost user=postgres password=postgres dbname=test sslmode=disable"
	log.Printf("connecting to pg, conf: %v", db)
	DB, err := gorm.Open("postgres", db)
	if err != nil {
		panic(err)
	}
	DB.DB().SetMaxOpenConns(20)
	models.DB = DB
}

func init() {
	log.Println("Init database")
	databaseInit()
	log.Println("Init models")
	models.Init()
	log.Println("success models")
	//configurate ransack
	config = &ransack.Configuration{}
	config.Models = []ransack.Model{}
	config.Models = append(config.Models, &models.User{}, &models.Book{}, &models.Recomendation{})
	config.DB = models.DB
	ransack.Config = *config
	ransack.Init()
	// config.Models = (&models.User{}, &models.Book{}, &models.Recomendation{})
}

func main() {

	r := gin.Default()
	r.NoRoute(controllers.NoRoute)
	r.GET("/", controllers.Index)
	r.GET("/show", controllers.Show)
	r.GET("/user/:id", controllers.UserShow)
	r.GET("/create_seeds", controllers.Seeds)
	testRoutes := r.Group("/v1")
	ransack.APIRoutes(testRoutes)
	ln, _ := net.Listen("tcp", ":3040")

	_, port, _ := net.SplitHostPort(ln.Addr().String())
	fmt.Println("Listening on port", port)

	http.Serve(ln, r)
}
