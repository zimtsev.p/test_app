package controllers

import (
	"log"
	"test/test_app/models"

	"github.com/gin-gonic/gin"
	"github.com/icrowley/fake"
	"github.com/jinzhu/gorm"
)

func Index(c *gin.Context) {
	var ids []int64
	err := models.DB.Table("users").Pluck("id", &ids).Error
	if err != nil {
		c.JSON(500, err)
	}
	c.JSON(200, gin.H{"message": "Hello, it's index",
		"UsersID": ids,
	})
}
func UserShow(c *gin.Context) {
	user := models.User{}
	err := models.DB.Preload("Book", func(db *gorm.DB) *gorm.DB { return db.Preload("Recommendations") }).Find(&user, "id = ?", c.Param("id")).Error
	if err != nil {
		c.JSON(500, err)
	}
	c.JSON(200, user)
}
func Show(c *gin.Context) {
	c.JSON(200, gin.H{"message": "it's show"})
}

func NoRoute(c *gin.Context) {
	c.JSON(404, gin.H{"message": "Currernt route is undefined"})
}

func Seeds(c *gin.Context) {
	userIDS := make([]int64, 0)
	bookIDS := make([]int64, 0)
	//user
	for i := 0; i < 5; i++ {
		user := models.User{
			Name:     fake.FirstName(),
			LastName: fake.LastName(),
			Age:      uint(20),
			Email:    fake.EmailAddress(),
			Phone:    fake.Phone()}
		err := models.DB.Create(&user).Error
		if err != nil {
			log.Println("Error create user ")
		} else {
			userIDS = append(userIDS, user.ID)
		}
	}
	log.Println(userIDS)
	//books
	for _, i := range userIDS {
		book := models.Book{
			UserID:      i,
			Title:       fake.Title(),
			Description: fake.Paragraphs()}
		err := models.DB.Create(&book).Error
		if err != nil {
			log.Println("Error create book ")
		} else {
			bookIDS = append(bookIDS, book.ID)
		}
	}
	//recommendations
	for _, i := range userIDS {
		for _, bookID := range bookIDS {
			book := models.Recomendation{
				UserID:  i,
				BookID:  bookID,
				Content: fake.Words()}
			err := models.DB.Create(&book).Error
			if err != nil {
				log.Println("Error create book ")
			}
		}
	}
	c.JSON(200, gin.H{"message": "seeds success"})
}
