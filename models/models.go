package models

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

var DB *gorm.DB

func Init() {
	err := DB.AutoMigrate(&User{}, &Book{}, &Recomendation{}).Error
	if err != nil {
		panic(err)
	}
	DB.Model(&Book{}).AddForeignKey("user_id", "users(id)", "CASCADE", "RESTRICT")
	DB.Model(&Recomendation{}).AddForeignKey("user_id", "users(id)", "CASCADE", "RESTRICT")
	DB.Model(&Recomendation{}).AddForeignKey("book_id", "books(id)", "CASCADE", "RESTRICT")

}

type User struct {
	ID       int64  `gorm:"primary_key" json:"id"`
	Name     string `json:"name"`
	LastName string `json:"last_name"`
	Age      uint   `json:"age"`
	Email    string `gorm:"unique_index,size:255" json:"email"`
	Phone    string `gorm:"index,size:255" json:"phone"`
	Book     *Book  `json:"book"`
}

type Book struct {
	ID              int64            `gorm:"primary_key" json:"id"`
	UserID          int64            `gorm:"type:bigint REFERENCES users(id)" json:"user_id"`
	User            *User            `json:"user"`
	Title           string           `json:"title"`
	Description     string           `json:"desctiption"`
	Recommendations []*Recomendation `json:"recommendations"`
}

type Recomendation struct {
	ID      int64  `gorm:"primary_key" json:"id"`
	UserID  int64  `gorm:"type:bigint REFERENCES users(id)" json:"user_id"`
	User    *User  `json:"user"`
	BookID  int64  `gorm:"type:bigint REFERENCES books(id)" json:"book_id"`
	Book    *Book  `json:"book"`
	Content string `json:"content"`
}

func (u *User) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []User{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

func (b *Book) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Book{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}

func (r *Recomendation) MakeResponse(total int, scope *gorm.DB, ctx *gin.Context) {
	array := []Recomendation{}
	if err := scope.Find(&array).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"total": total,
		"data":  array,
	})
	return
}
